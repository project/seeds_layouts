<?php

namespace Drupal\seeds_layouts\Plugin\LayoutField;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\seeds_layouts\Plugin\LayoutFieldBase;

/**
 * Provides a 'hide' field.
 *
 * @LayoutField(
 *   id = "hide",
 *   label = @Translation("Hide Section if Empty")
 * )
 */
class HideField extends LayoutFieldBase {

  /**
   * {@inheritDoc}
   */
  public function preprocess(&$variables) {
    if ($this->getConfiguration('hide')) {
      $children = Element::children($variables['content']);
      foreach ($children as $child) {
        $children_of_regions = Element::children($variables['content'][$child]);
        if (!empty($children_of_regions)) {
          foreach ($children_of_regions as $block_key) {
            $block = $variables['content'][$child][$block_key];
            if (!$block) {
              return;
            }
            $children_of_block = Element::children($block);
            if (!empty($children_of_block)) {
              return;
            }
          }
        }
      }

      $variables['attributes']['class'][] = 'hide-section';
      $variables['attributes']['style'][] = 'display:none;';
    }
  }

  /**
   * {@inheritDoc}.
   */
  public function build(array $form, FormStateInterface $form_state) {
    $form['hide'] = [
      '#type' => 'checkbox',
      '#title' => $this->getLabel(),
      '#default_value' => $this->getConfiguration('hide'),
    ];
    return $form;
  }

}
