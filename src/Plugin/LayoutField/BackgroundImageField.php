<?php

namespace Drupal\seeds_layouts\Plugin\LayoutField;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\file\FileInterface;
use Drupal\seeds_layouts\Plugin\LayoutFieldBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'background_image' field.
 *
 * @LayoutField(
 *   id = "background_image",
 *   label = @Translation("Background Image")
 * )
 */
class BackgroundImageField extends LayoutFieldBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManagar;

  /**
   * Constructs a new 'background_image' plugin.
   *
   * @param array $configuration
   *   The configuration.
   * @param $plugin_id
   *   The plugin id
   * @param $plugin_definition
   *   The plugin definition
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   *
   * @return \Drupal\seeds_layouts\Plugin\LayoutField\BackgroundImageField
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManagar = $entity_type_manager;
  }

  /**
   * {@inheritDoc}.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getAttributes() {
    $attributes = [];
    /** @var \Drupal\file\FileInterface $file */
    $file_value = !empty($this->getConfiguration('file')) ? $this->getConfiguration('file') : [];
    $file = $this->entityTypeManagar->getStorage('file')->load(reset($file_value));
    $uri = "";

    if ($file instanceof FileInterface) {
      if ($file->isTemporary()) {
        $file->setPermanent();
        $file->save();
      }
      $uri = $file->createFileUrl();
      $attributes['style'] = "background-image: url($uri);";
      $attributes['class'][] = 'seeds-layouts-background-image';
    }

    if ((bool) $this->getConfiguration('parallax')) {
      $attributes['class'][] = 'seeds-layouts-parallax';
    }

    if ((bool) $this->getConfiguration('repeat')) {
      $attributes['class'][] = 'repeat';
    }

    return $attributes;
  }

  /**
   *
   */
  public function getLibraries() {
    return ['seeds_layouts/parallax'];
  }

  /**
   * {@inheritDoc}.
   */
  public function getConfiguration($key = NULL) {
    $wrapper = parent::getConfiguration('wrapper');
    if ($key) {
      return $wrapper[$key] ?? NULL;
    }

    return $wrapper;
  }

  /**
   * {@inheritDoc}.
   */
  public function build(array $form, FormStateInterface $form_state) {

    $form['wrapper'] = [
      '#type' => 'details',
      '#title' => $this->getLabel(),
      '#tree' => TRUE,
    ];

    $form['wrapper']['file'] = [
      '#type' => 'managed_file',
      '#upload_location' => 'public://seeds_layouts',
      '#title' => t("Background Image"),
      '#default_value' => $this->getConfiguration('file'),
      '#process' => [
        '\\Drupal\\file\\Element\\ManagedFile::processManagedFile',
        '\\Drupal\\seeds_layouts\\Plugin\\LayoutField\\BackgroundImageField::processManagedFile',
      ],
    ];

    $form['wrapper']['parallax'] = [
      '#type' => 'checkbox',
      '#title' => t("Parallax"),
      '#default_value' => $this->getConfiguration('parallax'),
    ];

    $form['wrapper']['repeat'] = [
      '#type' => 'checkbox',
      '#title' => t("Repeat Background"),
      '#default_value' => $this->getConfiguration('repeat'),
    ];

    return $form;
  }

  /**
   * Render API callback: Add a submit on the remove button to set the file to temporary.
   */
  public static function processManagedFile(&$element, FormStateInterface $form_state, &$complete_form) {
    $element['remove_button']["#submit"][] = '\\Drupal\\seeds_layouts\\Plugin\\LayoutField\\BackgroundImageField::afterRemove';
    return $element;
  }

  /**
   * Set the file to temporary after removing the file.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return mixed
   */
  public static function afterRemove($form, FormStateInterface $form_state) {
    $parents = $form_state
      ->getTriggeringElement()['#array_parents'];
    array_pop($parents);
    $element = NestedArray::getValue($form, $parents);

    // Get files that will be removed.
    if (isset($element['#files'])) {
      $fids = array_keys($element['#files']);
      $fid = reset($fids);
      $file = \Drupal::entityTypeManager()->getStorage('file')->load($fid);
      if ($file instanceof FileInterface) {
        $file->setTemporary();
        $file->save();
      }
    }
  }

}
