<?php

namespace Drupal\seeds_layouts\Plugin\LayoutField;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\seeds_layouts\Plugin\LayoutFieldBase;

/**
 * Provides a 'wrapper' field.
 *
 * @LayoutField(
 *   id = "wrapper",
 *   label = @Translation("Wrapper")
 * )
 */
class WrapField extends LayoutFieldBase {

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
    $default = [];
    $regions = $this->layout->getRegionNames();
    foreach ($regions as $region) {
      $default["{$region}_wrap"] = FALSE;
      $default["{$region}_wrapper"] = "div";
      $default["{$region}_class"] = "";
    }
    return $default;
  }

  /**
   * Check if a string is uuid.
   *
   * @param string $string
   *   The string to check.
   *
   * @return bool
   */
  private function isUuid($string) {
    return preg_match("/^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i", $string);
  }

  /**
   * {@inheritDoc}
   */
  public function preprocess(&$variables) {
    $regions = $this->layout->getRegionNames();
    $add_wrapper = FALSE;
    foreach ($regions as $region) {
      if ($this->getConfiguration("{$region}_wrap")) {
        if (!isset($variables['content'][$region])) {
          continue;
        }

        $children = Element::children($variables['content'][$region]);

        // Do not work inside layout manage page, we don't want to mess up the drag-and-drop.
        // @todo Find a way to add the wrapper inside the layout builder manage page without affecting drag-and-drop.
        if (isset($variables['content'][$region]['layout_builder_add_block'])) {
          return;
        }

        $wrapper = $this->getConfiguration("{$region}_wrapper");

        $container = [
          '#type' => 'html_tag',
          '#tag' => $wrapper,
          '#attributes' => NestedArray::mergeDeep(['class' => [$region . '-wrapper', $this->getConfiguration("{$region}_class")]]),
        ];
        foreach ($children as $child) {
          // We check if the child is UUID to not affect anything beside actualy child blocks.
          if (!$this->isUuid($child)) {
            continue;
          }

          // If the any of the block has a child, add the wrapper.
          $children_of_block = Element::children($variables['content'][$region][$child]);
          if (!empty($children_of_block)) {
            $add_wrapper = TRUE;
          }

          $child_render = $variables['content'][$region][$child];
          unset($variables['content'][$region][$child]);
          $container['content'][] = $child_render;
        }

        if ($add_wrapper) {
          $variables['content'][$region]['wrapper'] = $container;
        }
      }
    }

  }

  /**
   * {@inheritDoc}.
   */
  public function build(array $form, FormStateInterface $form_state) {
    $form['#type'] = 'details';
    $form['#title'] = $this->getLabel();
    $form['#description'] = t('Keep in mind for now, you can\'t preview the changes here. Adding wrappers here will mess up the drag-and-drop functionalities.');

    $regions = $this->layout->getRegions();

    foreach ($regions as $region => $region_settings) {
      $region_label = $region_settings['label'];
      $wrap_field = "{$region}_wrap";
      $wrapper_field = "{$region}_wrapper";
      $class_field = "{$region}_class";

      $form[$wrap_field] = [
        '#type' => 'checkbox',
        '#title' => t('Wrap "@region" region?', ['@region' => $region_label]),
        '#default_value' => $this->getConfiguration($wrap_field),
      ];

      $wrap_checkbox_selector = ':input[name="' . $this->getRealName("{$region}_wrap") . '"]';

      $form[$wrapper_field] = [
        '#type' => 'textfield',
        '#title' => t('Wrapper for "@region"', ['@region' => $region_label]),
        '#default_value' => $this->getConfiguration($wrapper_field),
        '#states' => [
          'visible' => [
            $wrap_checkbox_selector => ['checked' => TRUE],
          ],
        ],
      ];

      $form[$class_field] = [
        '#type' => 'textfield',
        '#title' => t('Classes for "@region" wrapper', ['@region' => $region_label]),
        '#default_value' => $this->getConfiguration($class_field),
        '#states' => [
          'visible' => [
            $wrap_checkbox_selector => ['checked' => TRUE],
          ],
        ],
      ];
    }

    return $form;
  }

}
