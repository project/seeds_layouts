<?php

namespace Drupal\seeds_layouts\Plugin\LayoutField;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\seeds_layouts\Plugin\LayoutFieldBase;
use Drupal\seeds_layouts\SeedsLayoutsManager;

/**
 * Provides a 'wrapper' field for blocks.
 *
 * @LayoutField(
 *   id = "blocks_wrapper",
 *   label = @Translation("Blocks Wrapper (Alpha)")
 * )
 */
class WrapBlocksField extends LayoutFieldBase {

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
    $default = [];
    $regions = $this->layout->getRegionNames();
    foreach ($regions as $region) {
      $default[$region] = [];
    }
    return $default;
  }

  /**
   * {@inheritDoc}
   */
  public function preprocess(&$variables) {
    $regions = $this->layout->getRegionNames();
    $value = $this->getConfiguration('blocks_wrapper');
    foreach ($regions as $region_name) {
      $region = $value[$region_name] ?? [];
      if (!isset($variables['content'][$region_name])) {
        continue;
      }

      $region_build = &$variables['content'][$region_name];
      if (!$region_build) {
        continue;
      }

      if (empty($region)) {
        continue;
      }

      foreach ($region as $group) {
        $wrap_blocks = $group['blocks'] ?? [];

        $wrapper = [
          '#type' => 'html_tag',
          '#tag' => $group['wrapper'] ?? 'div',
          '#attributes' => SeedsLayoutsManager::attributesStringToArray($group['attributes'] ?? ''),
        ];

        $children_of_region = Element::children($region_build);
        foreach ($children_of_region as $uuid) {
          if (in_array($uuid, $wrap_blocks)) {
            $wrapper[$uuid] = $region_build[$uuid];
            if (isset($region_build[$uuid]['#weight'])) {
              $wrapper['#weight'] = $region_build[$uuid]['#weight'];
            }
            unset($region_build[$uuid]);
          }
        }
        $region_build[] = $wrapper;
      }
    }
  }

  /**
   * {@inheritDoc}.
   */
  public function build(array $form, FormStateInterface $form_state) {
    $form['#type'] = 'details';
    $form['#title'] = $this->getLabel();
    $form['#description'] = t('Keep in mind for now, you can\'t preview the changes here. Adding wrappers here will mess up the drag-and-drop functionalities.');
    $regions_options = [];
    $regions = $this->layout->getRegions();

    foreach ($regions as $region_name => $region) {
      $regions_options[$region_name] = $region['label'];
    }

    $form['blocks_wrapper'] = [
      '#type' => 'block_wrapper',
      '#regions' => $regions_options,
      '#default_value' => $this->getConfiguration('blocks_wrapper') ?? [],
    ];

    return $form;
  }

}
