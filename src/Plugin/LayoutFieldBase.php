<?php

namespace Drupal\seeds_layouts\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Base class for Layout Field plugins.
 */
abstract class LayoutFieldBase extends PluginBase implements LayoutFieldInterface {

  /**
   * The uuid of the field instance.
   *
   * @var string
   */
  protected $uuid;

  /**
   * The layout.
   *
   * @var \Drupal\Core\Layout\LayoutDefinition
   */
  protected $layout;

  /**
   * {@inheritDoc}
   */
  public function setUuid($uuid) {
    $this->uuid = $uuid;
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function setLayout($layout) {
    $this->layout = $layout;
  }

  /**
   * {@inheritDoc}.
   */
  public function getAttributes() {
    return [];
  }

  /**
   * {@inheritDoc}
   */
  public function preprocess(&$variables) {
    return;
  }

  /**
   * {@inheritDoc}.
   */
  public function build(array $form, FormStateInterface $form_state) {
    return [];
  }

  /**
   * {@inheritDoc}.
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * {@inheritDoc}.
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    return TRUE;
  }

  /**
   * {@inheritDoc}.
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritDoc}.
   */
  public function getPluginLabel() {
    return $this->getPluginDefinition()['label'];
  }

  /**
   * {@inheritDoc}.
   */
  public function getLabel() {
    return $this->configuration['description']['label'];
  }

  /**
   * {@inheritDoc}.
   */
  public function getConfiguration($key = NULL) {
    if ($key !== NULL) {
      return $this->configuration[$key] ?? NULL;
    }

    return $this->configuration;
  }

  /**
   * {@inheritDoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration;
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritDoc}.
   */
  public function getLibraries() {
    return [];
  }

  /**
   * Get the real name of the layout field.
   *
   * @param string $name
   *   The name of the layout field.
   *
   * @return string
   */
  protected function getRealName($name) {
    return "layout_settings[layout_fields][{$this->uuid}][$name]";
  }

}
