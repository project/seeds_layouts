<?php

namespace Drupal\seeds_layouts\Element;

use Drupal\Component\Serialization\Exception\InvalidDataTypeException;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\FormElement;

/**
 * Provides a custom element for blocks wrapper using React.
 *
 * @FormElement("block_wrapper")
 */
class BlocksWrapper extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = static::class;
    return [
      '#input' => TRUE,
      '#regions' => [],
      '#pre_render' => [
        [$class, 'preRenderWrapper'],
      ],
      '#theme' => 'input',
      '#theme_wrappers' => ['form_element'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    if ($input !== FALSE && $input !== NULL) {
      try {
        $decoded = Json::decode($input);
      }
      catch (InvalidDataTypeException $e) {
        $decoded = [];
      }

      return $decoded;
    }

    return [];
  }

  /**
   * Prerenders the element and set it up for the React application.
   *
   * @param array $element
   *   The element.
   *
   * @return array
   *   The $element with prepared variables.
   */
  public static function preRenderWrapper($element) {
    if (isset($element['#default_value'])) {
      try {
        $element['#value'] = Json::encode($element['#default_value']);
      }
      catch (InvalidDataTypeException $e) {
      }
    }
    $element['#attributes']['type'] = 'hidden';
    $element['#attributes']['id'] = 'blocks-wrapper-input';
    $element['#attributes']['class'] = 'blocks-wrapper-input';
    Element::setAttributes($element, ['name', 'value']);

    $container = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'blocks-wrapper',
        'data-regions' => json_encode($element['#regions']) ?? [],
      ],
    ];
    $element['#suffix'] = \Drupal::service('renderer')->render($container);
    $element['#attached']['library'][] = 'seeds_layouts/blocks_wrapper';
    return $element;
  }

}
