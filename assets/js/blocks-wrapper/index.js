import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import $ from "jquery";
(function ($, Drupal) {
  Drupal.behaviors.blocksWrapper = {
    attach: function (context, settings) {
      setTimeout(() => {
        const blocksWrapper = document.getElementById("blocks-wrapper");
        const blocksWrapperInput = document.getElementById(
          "blocks-wrapper-input"
        );
        const highlightedSection = $(".is-layout-builder-highlighted");
        if (blocksWrapper && highlightedSection.length) {
          const blockOptions = {};
          highlightedSection.find(".layout-builder-block").each((k, block) => {
            blockOptions[
              block.getAttribute("data-layout-builder-highlight-id")
            ] = block.getAttribute(
              "data-layout-content-preview-placeholder-label"
            );
          });

          let defaultValue = blocksWrapperInput.getAttribute("value");
          if (defaultValue) {
            try {
              defaultValue = JSON.parse(defaultValue);
            } catch (e) {
              defaultValue = null;
            }
          }

          ReactDOM.render(
            <App
              blocks={blockOptions}
              regions={JSON.parse(blocksWrapper.getAttribute("data-regions"))}
              defaultValue={defaultValue}
            />,

            blocksWrapper
          );
        }
      });
    },
  };
})(jQuery, Drupal);
