import styled from "styled-components";


export const Group = styled.div`
  #drupal-off-canvas &&& {
    display: block;
    padding: 5px 10px;
  }
`;

export const Label = styled.label`
  #drupal-off-canvas &&& {
    display: block;
    width: 100%;
    font-weight: bold;
    margin-bottom: 5px;
  }
`;

export const Input = styled.input`
  #drupal-off-canvas &&& {
    display: block;
    width: 100%;
    padding: 5px 10px;
    background-color: white;
  }
`;

export const Select = styled.select`
  #drupal-off-canvas &&& {
    display: block;
    width: 100%;
    padding: 5px 10px;
    background-color: white;
    overflow-y: scroll;
  }
`;

export const Option = styled.option`
  #drupal-off-canvas &&& {
    background-color: white;
    &:checked {
      background-color: #f2f2f2;
    }
  }
`;

export const AccordionTitle = styled.h2`
  #drupal-off-canvas &&& {
    cursor: pointer;
    display: flex;
    justify-content: space-between;
  }
  #drupal-off-canvas .regions-accordion &&& {
    font-size: 24px;
  }

  #drupal-off-canvas .group-accordion &&& {
    font-size: 16px;
  }
`;

export const DeleteGroup = styled.span`
  #drupal-off-canvas &&& {
    color: #00aaff;
    text-decoration: underline;
    cursor: pointer;
  }
`;

export const GroupsWrapper = styled.section`
  #drupal-off-canvas &&& {
    padding-left: 15px;
  }
`;

export const Button = styled.button`
  #drupal-off-canvas &&& {
    padding-left: 5px 10px;
    color: #00aaff;
    text-decoration: underline;
  }
`;
