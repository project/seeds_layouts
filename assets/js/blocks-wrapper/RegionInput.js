import React, { useEffect, useState } from "react";
import SimpleAccordion from "./SimpleAccordion";
import $ from "jquery";
import {generateUuid} from './helper';
import {
  Group,
  Input,
  Label,
  Select,
  Option,
  GroupsWrapper,
  Button,
} from "./styled";
const RegionInput = ({
  regionId,
  regionLabel,
  blocks = [],
  onChange,
  defaultValue,
}) => {
  const [groups, setGroups] = useState([]);

  useEffect(() => {
    onChange(regionId, groups);
  }, [groups]);

  useEffect(() => {
    if (defaultValue) {
      setGroups(defaultValue);
    }
  }, [defaultValue]);

  const addGroup = (e) => {
    e.preventDefault();
    const newGroups = groups.map((group) => ({ ...group }));
    newGroups.push({
      blocks: [],
      wrapper: "div",
      attributes: "",
      admin_label: null,
      id: generateUuid(),
    });
    setGroups(newGroups);
  };

  const removeGroup = (id) => {
    let newGroups = groups.map((group) => ({ ...group }));
    newGroups = newGroups.filter((group) => id !== group.id);
    setGroups(newGroups);
  };

  const handleChange = (groupId, e) => {
    let newGroups = groups.map((group) => ({ ...group }));
    const { name, value, selectedOptions, type } = e.target;
    const groupToEdit = newGroups.find((group) => group.id === groupId);
    if (type === "select-multiple") {
      let value = Array.from(selectedOptions, (option) => option.value);
      groupToEdit[name] = value;
    } else {
      groupToEdit[name] = value;
    }
    setGroups(newGroups);
  };

  return (
    <div className="blocks-wrapper-region">
      <SimpleAccordion label={regionLabel} classNames="regions">
        <GroupsWrapper>
          {groups.map((group) => (
            <SimpleAccordion
              key={group.id}
              label={group.admin_label ? group.admin_label : `Unnamed Group`}
              classNames="group"
              id={group.id}
              cancel
              onCancel={removeGroup}
            >
              <Group>
                <Label>{Drupal.t("Admin Label")}</Label>
                <Input
                  required
                  value={group.admin_label || ""}
                  name="admin_label"
                  type="text"
                  onChange={(e) => handleChange(group.id, e)}
                />
              </Group>
              <Group>
                <Label>{Drupal.t("Wrapper")}</Label>
                <Select
                  onChange={(e) => handleChange(group.id, e)}
                  required
                  value={group.wrapper || ""}
                  name="wrapper"
                >
                  <option value="div">div</option>
                  <option value="section">section</option>
                  <option value="article">article</option>
                </Select>
              </Group>
              <Group>
                <Label>{Drupal.t("Attributes")}</Label>
                <Input
                  onChange={(e) => handleChange(group.id, e)}
                  type="text"
                  name="attributes"
                  value={group.attributes || ""}
                />
              </Group>
              <Group>
                <Label>{Drupal.t("Blocks")}</Label>
                <Select
                  onChange={(e) => handleChange(group.id, e)}
                  required
                  name="blocks"
                  value={group.blocks}
                  multiple
                >
                  {$.map(blocks, (blockName, blockId) => (
                    <Option key={blockId} value={blockId}>
                      {blockName}
                    </Option>
                  ))}
                </Select>
              </Group>
            </SimpleAccordion>
          ))}
        </GroupsWrapper>
        <Button
          type="button button--primary js-form-submit form-submit"
          onClick={addGroup}
        >
          Add group
        </Button>
      </SimpleAccordion>
    </div>
  );
};

export default RegionInput;
