import React, { useEffect, useState } from "react";
import $ from "jquery";
import RegionInput from "./RegionInput";
import { Group } from "./styled";
const App = ({ blocks = {}, regions = {}, defaultValue = null }) => {
  const [value, setValue] = useState(defaultValue ? defaultValue : {});

  const handleRegionChange = (regionId, groups) => {
    const newValue = { ...value };
    newValue[regionId] = groups;
    setValue(newValue);
  };

  useEffect(() => {
    try {
      $("#blocks-wrapper-input").val(JSON.stringify(value));
    } catch (e) {
      console.error(e);
      $("#blocks-wrapper-input").val(JSON.stringify({}));
    }
  }, [value]);

  const defaultValueOfRegion = (regionId) => {
    if (defaultValue && defaultValue[regionId]) {
      return defaultValue[regionId];
    }

    return null;
  };

  return (
    <Group>
      {$.map(regions, (label, regionId) => (
        <RegionInput
          key={regionId}
          onChange={handleRegionChange}
          regionId={regionId}
          regionLabel={label}
          blocks={blocks}
          defaultValue={defaultValueOfRegion(regionId)}
        />
      ))}
    </Group>
  );
};

export default App;
