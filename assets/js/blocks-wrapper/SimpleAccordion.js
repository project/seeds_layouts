import React, { useState } from "react";
import { AccordionTitle, DeleteGroup } from "./styled";

const SimpleAccordion = ({
  children,
  label,
  classNames,
  cancel = false,
  id,
  onCancel,
}) => {
  const [visible, setVisible] = useState(false);
  return (
    <div className={`blocks-wrapper-simple-accordion ${classNames}-accordion`}>
      <AccordionTitle onClick={() => setVisible(!visible)}>
        {label}
        {cancel && <DeleteGroup onClick={() => onCancel(id)}>X</DeleteGroup>}
      </AccordionTitle>
      <div
        className={`accordion-content`}
        style={{ display: visible ? "block" : "none" }}
      >
        {children}
      </div>
    </div>
  );
};

export default SimpleAccordion;
